package main

import (
	"log"
	"net/http"

	"gitlab.com/birowo/oauth2-google/handlers"
)

func main() {
	// We create a simple server using http.Server and run.
	server := &http.Server{
		Addr:    ":8080",
		Handler: handlers.New(),
	}

	log.Printf("Starting HTTP Server. Listening at %q", server.Addr)
	if err := server.ListenAndServe(); err != nil {
		log.Println(err)
	}
}
