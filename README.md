* referensi: https://github.com/douglasmakey/oauth2-example

sebelum pembuatan kode aplikasi login with google perlu dibuat credential di google, dengan langkah-langkah :

1. login ke google

2. akses ke console.cloud.google.com/cloud-resource-manager , klik create project

![cloud-resource-manager](https://user-images.githubusercontent.com/21541959/206823654-37a4a0ea-7e0a-4e64-9378-2d8cfca4523a.png)

3. isi Project name , klik create

![projectcreate](https://user-images.githubusercontent.com/21541959/206823685-ed037850-d006-428d-9709-55456bfebd2d.png)

4. akses console.cloud.google.com/apis/credentials/consent , pilih External lalu klik CREATE

![consent](https://user-images.githubusercontent.com/21541959/206823697-603567ea-0b2a-4f0e-97b7-c7e8ddddbf73.png)
5. tentukan informasi yang akan ditampilkan ke user

![OAuthConsentScreen](https://user-images.githubusercontent.com/21541959/206823708-8f7f4d8f-f110-472f-b951-a2ca9e6e9f81.png)

6. pilih scopes , minimal userinfo.email

![scopes](https://user-images.githubusercontent.com/21541959/206823722-027af3c6-4ed8-4987-9b78-821cf74a116a.png)

7. pilih test users , isikan email-nya

![AddUsers](https://user-images.githubusercontent.com/21541959/206823734-d1ae183d-8d21-4c23-b936-909c34b2b9ca.png)

8. setelah selesai di OAuth consent screen , akses console.cloud.google.com/apis/credentials , klik CREATE CREDENTIALS , OAuth client ID

![OAuthClientID](https://user-images.githubusercontent.com/21541959/206823756-91f2ca0b-e153-42f7-9a73-47239803b9bc.png)

9. Isikan Application type , Name & Authorized redirect URIs

![CreateOAuthClientID](https://user-images.githubusercontent.com/21541959/206823786-b524222d-9ff1-4efc-8ee1-5113eec225f2.png)

10. download client_id , client_secret , dlsb. sebagai json file

![Credentials](https://user-images.githubusercontent.com/21541959/206823811-a649641f-7e3e-46e5-b2bc-3c1d5e772851.png)

11. setelah persiapan diatas selesai, bisa mulai pembuatan & mencoba kode aplikasi login with google

12. setelah pembuatan aplikasi login with google selesai, run aplikasi kemudian akses localhost:8080

![oauth2GoogleRun](https://user-images.githubusercontent.com/21541959/206837634-9335a79c-ca81-42e3-bdcb-4ea6ebd54ff4.png)

![loginWithGooglePilihAkun](https://user-images.githubusercontent.com/21541959/206828691-2e00aacb-420d-4f0d-bb7f-1058947e8c20.png)

13. jika login berhasil, bisa periksa bahwa aplikasi login with google sudah ada di myaccount.google.com/security , scroll ke bawah sampai di Login ke situs lain

![login_with_google](https://user-images.githubusercontent.com/21541959/206823827-80f246e3-87cc-45d8-af4f-1a0019b91710.png)

14. jika hendak mencoba mengulangi proses login , hapus akses di permission & hapus kuki lebih dahulu

![permissions_hapus_akses](https://user-images.githubusercontent.com/21541959/206823836-cbb11f3a-3c5b-4900-938b-9bfe2772a656.png)

![hapus_oauth2_kuki](https://user-images.githubusercontent.com/21541959/206829349-6092bac9-90a2-4d7b-9f36-51046ab6506a.png)
