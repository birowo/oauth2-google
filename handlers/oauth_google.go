package handlers

import (
	"crypto/rand"
	"encoding/base64"
	"io"
	"log"
	"net/http"
	"time"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

// Scopes: OAuth 2.0 scopes provide a way to limit the amount of access that is granted to an access token.
var googleOauthConfig = &oauth2.Config{
	RedirectURL:  "http://localhost:8080/auth/google/callback",
	ClientID:     "xxx.apps.googleusercontent.com",
	ClientSecret: "yyy",
	Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
	Endpoint:     google.Endpoint,
}

const authKuki = "auth"

func oauthGoogleLogin(w http.ResponseWriter, r *http.Request) {
	// Create oauthState cookie
	var expiration = time.Now().Add(time.Minute)
	bs := make([]byte, 18)
	rand.Read(bs)
	oauthState := base64.URLEncoding.EncodeToString(bs)
	cookie := http.Cookie{
		Name:     authKuki,
		Value:    oauthState,
		Expires:  expiration,
		Secure:   true,
		HttpOnly: true,
		SameSite: http.SameSiteLaxMode,
	}
	http.SetCookie(w, &cookie)
	/*
		AuthCodeURL receive state that is a token to protect the user from CSRF attacks. You must always provid>                validate that it matches the the state query parameter on your redirect callback.
	*/
	u := googleOauthConfig.AuthCodeURL(oauthState)
	http.Redirect(w, r, u, http.StatusTemporaryRedirect)
}

const oauthGoogleUrlAPI = "https://www.googleapis.com/oauth2/v2/userinfo?access_token="

func oauthGoogleCallback(w http.ResponseWriter, r *http.Request) {
	urlQuery := r.URL.Query()
	state, ok := urlQuery["state"]
	// Read oauthState from Cookie
	oauthState, err := r.Cookie(authKuki)
	if ok && err == nil && state[0] == oauthState.Value {
		if code, ok := urlQuery["code"]; ok {
			// Use code to get token and get user info from Google.
			var token *oauth2.Token
			if token, err = googleOauthConfig.Exchange(r.Context(), code[0]); err == nil {
				var response *http.Response
				if response, err = http.Get(oauthGoogleUrlAPI + token.AccessToken); err == nil {
					defer response.Body.Close()
					var contents []byte
					if contents, err = io.ReadAll(response.Body); err == nil {
						// GetOrCreate User in your db.
						// Redirect or response with a token.
						// More code .....
						w.Write(contents)
						return
					}
				}
			}
		}
	}
	log.Println(err.Error())
	http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
}
